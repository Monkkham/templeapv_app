import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PageHome extends StatefulWidget {
  const PageHome({Key? key}) : super(key: key);

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  late WebViewController controller;
  bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: AppBar(
        centerTitle: true,
        title: Text('ວັດອຳພາວັນ', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),),
      ),
      body: SafeArea(
        child: Stack(
          children: [
            WebView(
              initialUrl: 'https://templeapv.com/',
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (controller) {
                this.controller = controller;
              },
              onPageStarted: (url) {
                print('page Started : $url');
                Future.delayed(const Duration(milliseconds: 10), () {
                  setState(() {
                    isLoading = false;
                  });
                });
              },
            ),
            if (isLoading)
              const Center(
                child: SizedBox(
                  width: 70,
                  height: 70,
                  child: CircularProgressIndicator(),
                ),
              )
          ],
        ),
      ),
    );
  }
}
